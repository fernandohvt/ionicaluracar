export interface Agendamento {
  nomeCliente: string;
  enderecoCliente: string;
  emailCliente: string;
  mmodeloCarro: string;
  precoTotal: number;
  confirmado: boolean,
  enviado: boolean,
  data: string;
}
