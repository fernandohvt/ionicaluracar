import { AgendamentoDaoProvider } from './../../providers/agendamento-dao/agendamento-dao';
import { HomePage } from './../home/home';
import { AgendamentosServiceProvider } from './../../providers/agendamentos-service/agendamentos-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Alert } from 'ionic-angular';
import { Carro } from '../../modelos/carro';
import { Agendamento } from '../../modelos/agendamento';

/**
 * Generated class for the CadastroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html',
})
export class CadastroPage {

  public carro: Carro;
  public precoTotal: number;
  public nome: string = "";
  public endereco: string = "";
  public email: string = "";
  public data: string = new Date().toISOString();
  private alerta: Alert;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private _agendamentosService: AgendamentosServiceProvider,
    private _alertCtrl: AlertController, private _agendamentoDao: AgendamentoDaoProvider)

    {
      this.carro = this.navParams.get('carroSelecionado');
      this.precoTotal = this.navParams.get('precoTotal');
   }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CadastroPage');
  }

  agenda(){
    if(!this.nome || !this.endereco || !this.email){
      this._alertCtrl.create({
        title:'Preenchimento obrigatório',
        subTitle: 'Preencha todos os campos',
        buttons: [{text:'ok'}]
      }).present();
      return;
    }
    this.alerta = this._alertCtrl.create({
      title: 'Aviso',
      buttons: [{text:'ok', handler:()=>{this.navCtrl.setRoot(HomePage) }}]
    });
    let agendamento: Agendamento ={
        nomeCliente: this.nome,
        enderecoCliente: this.endereco,
        emailCliente: this.email,
        mmodeloCarro: this.carro.nome,
        precoTotal: this.precoTotal,
        confirmado: false,
        enviado: false,
        data: this.data
    }
    let mensagem = '';

    this._agendamentoDao.ehDuplicado(agendamento)
    .mergeMap(ehDuplicado => {
      if(ehDuplicado){
        throw new Error('Agendamento já existente');
      }
      return this._agendamentosService.agenda(agendamento)
    })
    .mergeMap( (valor)=>{
      let observable = this._agendamentoDao.salva(agendamento);
      if(valor instanceof Error){
        throw valor;
      }
      return observable;
    })
    .finally(
      () =>{
        this.alerta.setSubTitle(mensagem);
        this.alerta.present();
      }
    )
    .subscribe(()=>{
      mensagem = 'Agendamento realizado';
    },
    (err: Error)=>{
      mensagem = err.message;
    });

  }



}
