import { EscolhaPage } from './../escolha/escolha';
import { NavLifeCycles } from './../../utils/ionic/nav/nav-lifecycles';
import { CarrosServiceProvider } from './../../providers/carros-service/carros-service';
import { AppModule } from './../../app/app.module';
import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { Carro } from '../../modelos/carro';
import { HttpClient, HttpErrorResponse} from '@angular/common/http';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements NavLifeCycles{
  public carros: Carro[];

  constructor(public navCtrl: NavController,private _loadCtrl: LoadingController,
    private _alertCtrl: AlertController, private _navCtrl: NavController, public carService: CarrosServiceProvider) {



  }
  ionViewDidLoad(){
    let loading = this._loadCtrl.create({
      content: 'Carregando carros...'
    });
    loading.present();
   this.carService.lista()
        .subscribe((carros)=>{
            this.carros = carros;
            setTimeout(()=>{
              loading.dismiss();
            },500);

        },(err: HttpErrorResponse)=>{
          loading.dismiss();
          this._alertCtrl.create({
            title: 'Falha na conexão',
            subTitle: 'Não foi possível carregar a lista de carros',
            buttons: [{text: 'ok'}]
          }).present();

        });
  }

  selecionaCarro(carro: Carro){
    this.navCtrl.push(EscolhaPage.name,{carroSelecionado:carro});
  }

}
